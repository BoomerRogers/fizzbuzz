FizzBuzz
========
FizzBuzz is a program used in some programming interviews to test the skills of the candidates. I thought it would be fun to reproduce this small program. It's really just a small program to find all the 5's and 3's in the world.
